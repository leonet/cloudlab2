from flask import Flask
#from flask_restplus import Resource, Api 
import math
from math import *
#import jsonpickle

def integration(a, b):
    a=float(a)
    b=float(b)
    I=[]
    for N in ([1, 10, 100, 1000, 10000, 100000, 1000000]):
        dx = (b-a)/N
        x=a
        Integral = 0
        for i in range(N):
            Integral += abs(math.sin(x))*dx
            x+=dx
        I.append(Integral)
    return I

app = Flask(__name__)
@app.route('/numericalintegralservice/<lower>/<upper>')
#@app.route('/numericalintegralservice/0/3.14')
#@api.doc(params={'lower','upper'})
#class TestApp(Resource):

def get(lower,upper): 
    intg=integration(lower,upper)
    return intg 
if __name__ == "__main__":
    app.run()



