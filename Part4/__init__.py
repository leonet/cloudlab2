import logging
import math
import azure.functions as func

def integration(a, b):
    I=[]
    for N in ([1, 10, 100, 1000, 10000, 100000, 1000000]):
        dx = (b-a)/N
        x=a
        Integral = 0
        for i in range(N):
            Integral += abs(math.sin(x))*dx
            x+=dx
        I.append(Integral)
    return I

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('lower')
    upper = req.params.get('upper')

    if not lower:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            lower = req_body.get('lower')

    if not upper:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            upper = req_body.get('upper')

    if upper and lower:
        result = integration(float(lower), float(upper))
        return func.HttpResponse(f"The results is : {result}")
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass two parameters in the query string or in the request body for a personalized response.",
             status_code=200
        )